{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit fpcunit_pas2js_namespaced;

{$warn 5023 off : no warning about unused units}
interface

uses
  FpcUnit.Test, FpcUnit.Registry, FpcUnit.Decorator, FpcUnit.Runners.Console, FpcUnit.Report, FpcUnit.Reports.Plain, 
  FpcUnit.Runners.Browser, FpcUnit.Reports.HTML;

implementation

end.
